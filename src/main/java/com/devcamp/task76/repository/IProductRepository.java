package com.devcamp.task76.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task76.entity.Product;

public interface IProductRepository extends JpaRepository<Product, Integer> {

}
