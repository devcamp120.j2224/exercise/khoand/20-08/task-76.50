package com.devcamp.task76.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task76.entity.ProductLine;

public interface IProductLineRepository extends JpaRepository<ProductLine, Integer> {

}
