package com.devcamp.task76.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task76.entity.Order;

public interface IOrderRepository extends JpaRepository<Order, Integer> {

}
