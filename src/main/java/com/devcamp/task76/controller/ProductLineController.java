package com.devcamp.task76.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.task76.entity.ProductLine;
import com.devcamp.task76.repository.IProductLineRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProductLineController {
   @Autowired
   private IProductLineRepository productLineRepository;

   @GetMapping("/product-lines")
   public ResponseEntity<List<ProductLine>> getAllProductLine() {
      try {
         List<ProductLine> productLineList = new ArrayList<ProductLine>();

         productLineRepository.findAll().forEach(productLineList::add);

         return new ResponseEntity<>(productLineList, HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/product-lines/{id}")
   public ResponseEntity<ProductLine> getProductLineById(@PathVariable("id") int id) {
      Optional<ProductLine> productLineData = productLineRepository.findById(id);
      if (productLineData.isPresent()) {
         return new ResponseEntity<>(productLineData.get(), HttpStatus.OK);
      } else {
         return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
   }

   @PostMapping("/product-lines")
   public ResponseEntity<Object> createProductLine(@RequestBody ProductLine pProductLine) {
      try {
         ProductLine newProductLine = new ProductLine();
         newProductLine.setProductLine(pProductLine.getProductLine());
         newProductLine.setDescription(pProductLine.getDescription());
         productLineRepository.save(newProductLine);
         return new ResponseEntity<>(newProductLine, HttpStatus.CREATED);
      } catch (Exception e) {
         System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
         // Hiện thông báo lỗi tra back-end
         return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
         // return ResponseEntity.unprocessableEntity().body("Failed to Create specified
         // Voucher: "+e.getCause().getCause().getMessage());
      }
   }

   @PutMapping("/product-lines/{id}")
   public ResponseEntity<Object> updateProductLineById(@PathVariable("id") int id,
         @RequestBody ProductLine pProductLine) {
      Optional<ProductLine> productLineData = productLineRepository.findById(id);
      if (productLineData.isPresent()) {
         ProductLine productLine = productLineData.get();
         productLine.setProductLine(pProductLine.getProductLine());
         productLine.setDescription(pProductLine.getDescription());
         try {
            return new ResponseEntity<>(productLineRepository.save(productLine), HttpStatus.OK);
         } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                  .body("Failed to Update specified Voucher:" + e.getCause().getCause().getMessage());
         }

      } else {
         // return new ResponseEntity<>(HttpStatus.NOT_FOUND);
         return ResponseEntity.badRequest().body("Failed to get specified Voucher: " + id + "  for update.");
      }
   }

   @DeleteMapping("/product-lines/{id}")
   public ResponseEntity<ProductLine> deleteProductLineById(@PathVariable("id") int id) {
      try {
         productLineRepository.deleteById(id);
         return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
