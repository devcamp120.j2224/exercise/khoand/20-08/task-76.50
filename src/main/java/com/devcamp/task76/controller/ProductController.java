package com.devcamp.task76.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task76.entity.Product;
import com.devcamp.task76.entity.ProductLine;
import com.devcamp.task76.repository.IProductLineRepository;
import com.devcamp.task76.repository.IProductRepository;

@RestController
@RequestMapping("product")
public class ProductController {

    @Autowired
    IProductRepository iProduct;

    @Autowired
    IProductLineRepository iProductLine;

    @GetMapping("/all")
    public ResponseEntity<List<Product>> getProducts() {

        try {
            List<Product> listProduct = new ArrayList<Product>();
            iProduct.findAll().forEach(listProduct::add);

            if (listProduct.size() == 0) {
                return new ResponseEntity<List<Product>>(listProduct, HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<List<Product>>(listProduct, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/detail")
    public ResponseEntity<Object> getProductById(@PathVariable(name = "id", required = true) int id) {
        Optional<Product> ProductFounded = iProduct.findById(id);
        if (ProductFounded.isPresent())
            return new ResponseEntity<Object>(ProductFounded, HttpStatus.OK);
        else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    // tạo region bởi id của country
    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createRegion(@PathVariable(name = "id") int id, @RequestBody Product product) {

        Optional<ProductLine> productLineData = iProductLine.findById(id);
        try {
            if (productLineData.isPresent()) {

                Product newRole = new Product();
                newRole.setProductCode(product.getProductCode());
                newRole.setProductName(product.getProductName());
                newRole.setProductDescription(product.getProductDescription());
                newRole.setProductScale(product.getProductScale());
                newRole.setProductVendor(product.getProductVendor());
                newRole.setQuantityInStock(product.getQuantityInStock());
                newRole.setBuyPrice(product.getBuyPrice());

                ProductLine productLine = productLineData.get();
                newRole.setProductLine(productLine);

                Product savedRole = iProduct.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);

            }

            else {
                return ResponseEntity.unprocessableEntity().body(" asdasdasdsa ");
            }

        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity().body(" " + e.getCause().getCause().getMessage());
        }

    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateProductById(@PathVariable(name = "id") int id, @RequestBody Product Product) {
        Optional<Product> ProductData = iProduct.findById(id);
        if (ProductData.isPresent()) {
            Product saveProduct = ProductData.get();

            saveProduct.setProductCode(Product.getProductCode());
            saveProduct.setProductName(Product.getProductName());
            saveProduct.setProductDescription(Product.getProductDescription());
            saveProduct.setProductScale(Product.getProductScale());
            saveProduct.setProductVendor(Product.getProductVendor());
            saveProduct.setQuantityInStock(Product.getQuantityInStock());
            saveProduct.setBuyPrice(Product.getBuyPrice());

            try {
                return ResponseEntity.ok(iProduct.save(saveProduct));
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Product> deleteProductById(@PathVariable(name = "id") int id) {
        try {
            iProduct.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
