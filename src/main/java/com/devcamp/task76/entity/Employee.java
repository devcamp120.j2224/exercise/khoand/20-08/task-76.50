package com.devcamp.task76.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employees")
public class Employee {
   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private int id;

   @Column(name = "last_name")
   private String lastName;

   @Column(name = "first_name")
   private String firstName;

   private String extension;

   @Column(unique = true)
   private String email;

   @Column(name = "office_code")
   private int officeCode;

   @Column(name = "report_to")
   private int reportTo;

   @Column(name = "job_title")
   private String jobTitle;

   public Employee() {
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getLastName() {
      return lastName;
   }

   public void setLastName(String lastName) {
      this.lastName = lastName;
   }

   public String getFirstName() {
      return firstName;
   }

   public void setFirstName(String firstName) {
      this.firstName = firstName;
   }

   public String getExtension() {
      return extension;
   }

   public void setExtension(String extension) {
      this.extension = extension;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public int getOfficeCode() {
      return officeCode;
   }

   public void setOfficeCode(int officeCode) {
      this.officeCode = officeCode;
   }

   public int getReportTo() {
      return reportTo;
   }

   public void setReportTo(int reportTo) {
      this.reportTo = reportTo;
   }

   public String getJobTitle() {
      return jobTitle;
   }

   public void setJobTitle(String jobTitle) {
      this.jobTitle = jobTitle;
   }

}
