package com.devcamp.task76.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "customers")
public class Customer {

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private int id;

   private String address;

   private String city;

   private String country;

   @Column(name = "credit_limit")
   private int creditLimit;

   @Column(name = "first_name")
   private String firstName;

   @Column(name = "last_name")
   private String lastName;

   @Column(name = "phone_number")
   private String phoneNumber;

   @Column(name = "postal_code")
   private String postalCode;

   @Column(name = "sales_rep_employee_number")
   private int salesRepEmployeeNumber;

   private String state;

   @OneToMany(mappedBy = "customer")
   private List<Order> orders;

   @OneToMany(mappedBy = "customer")
   private List<Payment> payments;

   public Customer() {
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getAddress() {
      return address;
   }

   public void setAddress(String address) {
      this.address = address;
   }

   public String getCity() {
      return city;
   }

   public void setCity(String city) {
      this.city = city;
   }

   public String getCountry() {
      return country;
   }

   public void setCountry(String country) {
      this.country = country;
   }

   public int getCreditLimit() {
      return creditLimit;
   }

   public void setCreditLimit(int creditLimit) {
      this.creditLimit = creditLimit;
   }

   public String getFirstName() {
      return firstName;
   }

   public void setFirstName(String firstName) {
      this.firstName = firstName;
   }

   public String getLastName() {
      return lastName;
   }

   public void setLastName(String lastName) {
      this.lastName = lastName;
   }

   public String getPhoneNumber() {
      return phoneNumber;
   }

   public void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
   }

   public String getPostalCode() {
      return postalCode;
   }

   public void setPostalCode(String postalCode) {
      this.postalCode = postalCode;
   }

   public int getSalesRepEmployeeNumber() {
      return salesRepEmployeeNumber;
   }

   public void setSalesRepEmployeeNumber(int salesRepEmployeeNumber) {
      this.salesRepEmployeeNumber = salesRepEmployeeNumber;
   }

   public String getState() {
      return state;
   }

   public void setState(String state) {
      this.state = state;
   }

   public List<Order> getOrders() {
      return orders;
   }

   public void setOrders(List<Order> orders) {
      this.orders = orders;
   }

   public List<Payment> getPayments() {
      return payments;
   }

   public void setPayments(List<Payment> payments) {
      this.payments = payments;
   }
}