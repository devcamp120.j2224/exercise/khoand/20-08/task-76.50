package com.devcamp.task76.entity;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "products")
public class Product {
   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private int id;

   @Column(name = "product_code", unique = true)
   private String productCode;

   @Column(name = "product_name")
   private String productName;

   @Column(name = "product_description")
   private String productDescription;

   @Column(name = "product_scale")
   private String productScale;

   @Column(name = "product_vendor")
   private String productVendor;

   @Column(name = "quantity_in_stock")
   private int quantityInStock;

   @Column(name = "buy_price")
   private BigDecimal buyPrice;

   @OneToMany(mappedBy = "product")
   private Set<OrderDetail> orderDetails;

   @ManyToOne
   private ProductLine productLine;

   public Product() {
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getProductCode() {
      return productCode;
   }

   public void setProductCode(String productCode) {
      this.productCode = productCode;
   }

   public String getProductName() {
      return productName;
   }

   public void setProductName(String productName) {
      this.productName = productName;
   }

   public String getProductDescription() {
      return productDescription;
   }

   public void setProductDescription(String productDescription) {
      this.productDescription = productDescription;
   }

   public String getProductScale() {
      return productScale;
   }

   public void setProductScale(String productScale) {
      this.productScale = productScale;
   }

   public String getProductVendor() {
      return productVendor;
   }

   public void setProductVendor(String productVendor) {
      this.productVendor = productVendor;
   }

   public int getQuantityInStock() {
      return quantityInStock;
   }

   public void setQuantityInStock(int quantityInStock) {
      this.quantityInStock = quantityInStock;
   }

   public BigDecimal getBuyPrice() {
      return buyPrice;
   }

   public void setBuyPrice(BigDecimal buyPrice) {
      this.buyPrice = buyPrice;
   }

   public Set<OrderDetail> getOrderDetails() {
      return orderDetails;
   }

   public void setOrderDetails(Set<OrderDetail> orderDetails) {
      this.orderDetails = orderDetails;
   }

   public ProductLine getProductLine() {
      return productLine;
   }

   public void setProductLine(ProductLine productLine) {
      this.productLine = productLine;
   }

}
